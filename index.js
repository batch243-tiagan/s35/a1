const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243-tiagan.mtb2jt6.mongodb.net/?retryWrites=true&w=majority", 
	{ 
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);

let db = mongoose.connection; 

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const activitySchema = new mongoose.Schema({ 
	username : String,
	password : String

})

const Activity = mongoose.model("Activity", activitySchema); 

app.use(express.json());
app.use(express.urlencoded({extended:true})); 

app.post("/signup", (req, res)=> {

	Activity.findOne({username : req.body.username}, (err, result) => {

		if(result != null && result.username == req.body.username){

			return res.send("Duplicate user was found");

		} else {

			let newUser= new Activity({
				username : req.body.username,
				password : req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user created");

				}
			})
		}

	})
})

app.get("/users",(req,res) => {

		Activity.find({}, (err,result) => {
			if(err){
				return console.log(err);
			}else{
				return res.status(200).json({
					data:result
				})
			}
		})
})



app.listen(port, () => console.log(`Server running ${port}`))